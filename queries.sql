-- Zadanie 1
CREATE TABLE dragons (
  dragon_id int(11) NOT NULL,
  name varchar(45) NOT NULL,
  color varchar(45) NOT NULL,
  wingspan int(11) NOT NULL,
  PRIMARY KEY (dragon_id)
);

CREATE TABLE eggs (
  egg_id int(11) NOT NULL,
  weight int(11) NOT NULL,
  diameter int(11) NOT NULL,
  dragon_id int(11) NOT NULL,
  PRIMARY KEY (egg_id),
  KEY FK_eggs_dragons_idx (dragon_id),
  CONSTRAINT FK_eggs_dragons FOREIGN KEY (dragon_id) REFERENCES dragons (dragon_id)
);

CREATE TABLE adornments (
  egg_id int(11) NOT NULL,
  color varchar(45) NOT NULL,
  pattern varchar(45) NOT NULL,
  PRIMARY KEY (egg_id),
  CONSTRAINT FK_adornments_eggs FOREIGN KEY (egg_id) REFERENCES eggs (egg_id)
);

CREATE TABLE lands (
  land_id int(11) NOT NULL,
  name varchar(45) NOT NULL,
  PRIMARY KEY (land_id)
);

CREATE TABLE dragons_lands (
  dragon_id int(11) NOT NULL,
  land_id int(11) NOT NULL,
  PRIMARY KEY (dragon_id,land_id),
  KEY FK_dragons_lands_lands_idx (land_id),
  CONSTRAINT FK_dragons_lands_dragons FOREIGN KEY (dragon_id) REFERENCES dragons (dragon_id),
  CONSTRAINT FK_dragons_lands_lands FOREIGN KEY (land_id) REFERENCES lands (land_id)
);

-- Zadanie 2

SET @dragon_id = (SELECT IFNULL(MAX(dragon_id), 0) + 1 FROM dragons);
SET @egg_id = (SELECT IFNULL(MAX(egg_id), 0) + 1 FROM eggs);

-- Dygir
INSERT INTO dragons(dragon_id, name, color, wingspan)
VALUES (@dragon_id, 'Dygir', 'green', 200);

INSERT INTO eggs(egg_id, weight, diameter, dragon_id)
VALUES (@egg_id, 300, 20, @dragon_id);

INSERT INTO adornments(egg_id, color, pattern)
VALUES (@egg_id, 'pink', 'mesh');

SET @egg_id = @egg_id + 1;

INSERT INTO eggs(egg_id, weight, diameter, dragon_id)
VALUES (@egg_id, 340, 30, @dragon_id);

INSERT INTO adornments(egg_id, color, pattern)
VALUES (@egg_id, 'black', 'striped');

SET @egg_id = @egg_id + 1;

SET @dragon_id = @dragon_id + 1;
-- Bondril
INSERT INTO dragons(dragon_id, name, color, wingspan)
VALUES (@dragon_id, 'Bondril', 'red', 100);

INSERT INTO eggs(egg_id, weight, diameter, dragon_id)
VALUES (@egg_id, 200, 10, @dragon_id);

INSERT INTO adornments(egg_id, color, pattern)
VALUES (@egg_id, 'yellow', 'dotted');

SET @egg_id = @egg_id + 1;
SET @dragon_id = @dragon_id + 1;
-- Onosse
INSERT INTO dragons(dragon_id, name, color, wingspan)
VALUES (@dragon_id, 'Onosse', 'black', 250);

INSERT INTO eggs(egg_id, weight, diameter, dragon_id)
VALUES (@egg_id, 230, 20, @dragon_id);

INSERT INTO adornments(egg_id, color, pattern)
VALUES (@egg_id, 'blue', 'dotted');

SET @egg_id = @egg_id + 1;

INSERT INTO eggs(egg_id, weight, diameter, dragon_id)
VALUES (@egg_id, 300, 25, @dragon_id);

INSERT INTO adornments(egg_id, color, pattern)
VALUES (@egg_id, 'green', 'striped');

SET @egg_id = @egg_id + 1;
INSERT INTO eggs(egg_id, weight, diameter, dragon_id)
VALUES (@egg_id, 200, 15, @dragon_id);

INSERT INTO adornments(egg_id, color, pattern)
VALUES (@egg_id, 'red', 'mesh');

SET @egg_id = @egg_id + 1;
SET @dragon_id = @dragon_id + 1;
-- Chiri
INSERT INTO dragons(dragon_id, name, color, wingspan)
VALUES (@dragon_id, 'Chiri', 'yellow', 50);

SET @dragon_id = @dragon_id + 1;
-- Lase
INSERT INTO dragons(dragon_id, name, color, wingspan)
VALUES (@dragon_id, 'Lase', 'blue', 300);

-- ------------------ LANDS -----------------
SET @land_id = (SELECT IFNULL(MAX(land_id), 0) + 1 FROM lands);

-- Froze
INSERT INTO lands(land_id, name)
VALUES (@land_id, 'Froze');

INSERT INTO dragons_lands(dragon_id, land_id)
SELECT
	dragon_id,
    @land_id
FROM
	dragons
WHERE
	dragons.name IN ('Dygir', 'Bondril');

SET @land_id = @land_id + 1;
-- Oswia
INSERT INTO lands(land_id, name)
VALUES (@land_id, 'Oswia');

INSERT INTO dragons_lands(dragon_id, land_id)
SELECT
	dragon_id,
    @land_id
FROM
	dragons
WHERE
	dragons.name IN ('Dygir', 'Onosse', 'Chiri');

SET @land_id = @land_id + 1;
-- Oswia
INSERT INTO lands(land_id, name)
VALUES (@land_id, 'Oscyae');

SET @land_id = @land_id + 1;
-- Oswia
INSERT INTO lands(land_id, name)
VALUES (@land_id, 'Oclurg');

-- Zadanie 3
CREATE VIEW eggs_with_adornments AS
SELECT
	eggs.egg_id,
    eggs.weight,
    eggs.diameter,
    adornments.color,
    adornments.pattern
FROM
	eggs
    INNER JOIN
    adornments ON eggs.egg_id = adornments.egg_id;

CREATE VIEW dragons_and_lands_names AS
SELECT
	dragons.name AS dragon_name,
    lands.name AS land_name
FROM
	dragons
    INNER JOIN
    dragons_lands ON dragons_lands.dragon_id = dragons.dragon_id
    INNER JOIN
    lands ON lands.land_id = dragons_lands.land_id;
    
CREATE VIEW dragons_and_eggs AS
SELECT
	dragons.name AS dragon_name,
    eggs.weight AS egg_weight,
    eggs.diameter AS egg_diameter
FROM
	dragons
    INNER JOIN
    eggs ON eggs.dragon_id = dragons.dragon_id;
    
CREATE VIEW dragons_without_eggs AS
SELECT
	dragons.name
FROM
	dragons
    LEFT JOIN
    eggs ON eggs.dragon_id = dragons.dragon_id
WHERE
	eggs.egg_id IS NULL;

SELECT * FROM dragons;

SELECT * FROM eggs;

SELECT * FROM adornments;

SELECT * FROM lands;

SELECT * FROM dragons_lands;

SELECT * FROM eggs_with_adornments;

SELECT * FROM dragons_and_lands_names;

SELECT * FROM dragons_and_eggs;

SELECT * FROM dragons_without_eggs;

-- Zadanie 4
SELECT
	dragons.name
FROM
	dragons
WHERE
	dragons.wingspan BETWEEN 200 AND 400;
    
-- Zadanie 5
DROP VIEW dragons_without_eggs;
DROP VIEW dragons_and_lands_names;
DROP VIEW eggs_with_adornments;
DROP VIEW dragons_and_eggs;

DROP TABLE dragons_lands;
DROP TABLE lands;
DROP TABLE adornments;
DROP TABLE eggs;
DROP TABLE dragons;