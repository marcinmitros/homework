package pl.codementors.bar;

public class Client implements Runnable {
    private String name;
    private Bar bar;

    public Client(String name, Bar bar) {
        this.name = name;
        this.bar = bar;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                String drink = bar.requestDrink();
                for (int i = 0; i < drink.length(); i++) {
                    System.out.println(name + " is drinking: " + drink.substring(0, i + 1));
                    Thread.sleep(150);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        System.out.println(name + " has to go home");
    }
}
