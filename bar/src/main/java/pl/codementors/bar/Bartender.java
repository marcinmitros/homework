package pl.codementors.bar;

import java.util.Random;

public class Bartender implements Runnable {
    private final static String[] DRINKS = {"Mojito", "Pinacolada", "Bloody Mary"};
    private Bar bar;

    public Bartender(Bar bar) {
        this.bar = bar;
    }

    @Override
    public void run() {
        // Chwilka pauzy - barman zacznie pracę sekundę po wystartowaniu wątku.
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // Gdyby wątek został przerwany, ustawimy flagę
            Thread.currentThread().interrupt();
        }

        while (!Thread.currentThread().isInterrupted()) {
            try {
                bar.placeDrinkAtTheCounter(getRandomDrink());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        System.out.println("Bartender goes home");
    }

    private String getRandomDrink() {
        Random random = new Random();
        int randomDrinkIndex = random.nextInt(DRINKS.length);
        return DRINKS[randomDrinkIndex];
    }
}