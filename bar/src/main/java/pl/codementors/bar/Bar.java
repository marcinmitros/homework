package pl.codementors.bar;

public class Bar {
    private String drinkAtTheCounter;

    public synchronized String requestDrink() throws InterruptedException {
        // Zmiennej drinkAtTheCounter przypisujemy null
        String drink = null;
        // Metoda wait musi zostać użyta w pętli while (wątek może się wybudzić sam!)
        // Póki do zmiennej drinkAtTheCounter nie mamy przypisanej instancji
        while (drink == null) {
            // śpimy
            wait();
            // po wybudzeniu próbujemy z kolejki drinks wybrać pierwszy dodany do listy drinks element
            // (metoda poll jeśli kolejka jest pusta zwróci null, w innym wypadku zwróci pierwszy dodany
            // do kolejki element i go z tej kolejki usunie). Dzięki temu, że użyliśmy klasy reprezentującej
            // kolejkę obsługującą wielowątkowość nie mamy obawy o to, ze jednocześnie dwa wątki
            // wybiorą ten sam drinkAtTheCounter.
            drink = drinkAtTheCounter;
            drinkAtTheCounter = null;
        }
        return drink;
    }

    public synchronized void placeDrinkAtTheCounter(String drink) {
        drinkAtTheCounter = drink;
        notifyAll();
    }
}
