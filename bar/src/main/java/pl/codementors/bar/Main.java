package pl.codementors.bar;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Bar bar = new Bar();

        List<Thread> clientThreads = Arrays.asList(
                new Thread(new Client("John", bar)),
                new Thread(new Client("George", bar)),
                new Thread(new Client("Danny", bar)),
                new Thread(new Client("Juan", bar)),
                new Thread(new Client("Miguel", bar))
        );
        for (Thread clientThread : clientThreads) {
            clientThread.start();
        }
        Thread barmanThread = new Thread(new Bartender(bar));
        barmanThread.start();

        Scanner scanner = new Scanner(System.in);
        System.out.println("To stop press the \"Enter\" key");
        scanner.nextLine();

        for (Thread clientThread : clientThreads) {
            clientThread.interrupt();
        }
        barmanThread.interrupt();
    }
}
