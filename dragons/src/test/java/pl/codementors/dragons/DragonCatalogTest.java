package pl.codementors.dragons;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DragonCatalogTest {
    private DragonCatalog dragonCatalog;
    private Dragon dygir;
    private Dragon bondril;
    private Dragon onosse;
    private Dragon chiri;
    private Dragon lase;


    @Before
    public void setUp() {
        dragonCatalog = new DragonCatalog();
        dygir = new Dragon("Dygir", 30, "green", 200);
        dragonCatalog.add(dygir);
        bondril = new Dragon("Bondril", 134, "red", 100);
        dragonCatalog.add(bondril);
        onosse = new Dragon("Onosse", 245, "black", 250);
        dragonCatalog.add(onosse);
        chiri = new Dragon("Chiri", 10, "yellow", 50);
        dragonCatalog.add(chiri);
        lase = new Dragon("Lase", 560, "blue", 30);
        dragonCatalog.add(lase);
    }

    @Test
    public void getOldestDragonTest() {
        // when
        Optional<Dragon> oldestDragonOptional = dragonCatalog.getOldestDragon();
        // then
        assertTrue(oldestDragonOptional.isPresent());
        assertEquals(lase, oldestDragonOptional.get());
    }

    @Test
    public void getMaxWingspanTest() {
        // when
        OptionalInt maxWingSpanOptional = dragonCatalog.getMaxWingspan();
        // then
        assertTrue(maxWingSpanOptional.isPresent());
        assertEquals(250, maxWingSpanOptional.getAsInt());
    }

    @Test
    public void getMaxNameLengthTest() {
        // when
        OptionalInt maxNameLengthOptional = dragonCatalog.getMaxNameLength();
        // then
        assertTrue(maxNameLengthOptional.isPresent());
        assertEquals(7, maxNameLengthOptional.getAsInt());
    }

    @Test
    public void getSortedByAgeTest() {
        // when
        List<Dragon> sortedByAge = dragonCatalog.getSortedByAge();
        // then
        assertEquals(chiri, sortedByAge.get(0));
        assertEquals(dygir, sortedByAge.get(1));
        assertEquals(bondril, sortedByAge.get(2));
        assertEquals(onosse, sortedByAge.get(3));
        assertEquals(lase, sortedByAge.get(4));
    }

    @Test
    public void getByColorTest() {
        // when
        List<Dragon> byColor = dragonCatalog.getByColor("blue");
        // then
        assertEquals(1, byColor.size());
        assertEquals(lase, byColor.get(0));
    }

    @Test
    public void getNamesTest() {
        // when
        List<String> names = dragonCatalog.getNames();
        // then
        assertEquals(5, names.size());
        assertTrue(names.contains("Dygir"));
        assertTrue(names.contains("Bondril"));
        assertTrue(names.contains("Onosse"));
        assertTrue(names.contains("Chiri"));
        assertTrue(names.contains("Lase"));
    }


    @Test
    public void getColorToUpperCaseTest() {
        // when
        List<String> colorsToUpperCase = dragonCatalog.getColorsToUpperCase();
        // then
        assertEquals(5, colorsToUpperCase.size());
        assertTrue(colorsToUpperCase.contains("GREEN"));
        assertTrue(colorsToUpperCase.contains("RED"));
        assertTrue(colorsToUpperCase.contains("BLACK"));
        assertTrue(colorsToUpperCase.contains("YELLOW"));
        assertTrue(colorsToUpperCase.contains("BLUE"));
    }

    @Test
    public void areAllOlderThanTest() {
        // when
        boolean allOlderThan = dragonCatalog.areAllOlderThan(5);
        // then
        assertTrue(allOlderThan);
    }

    @Test
    public void isAnyOfColorTest() {
        // then
        boolean anyOfColor = dragonCatalog.isAnyOfColor("cyan");
        // then
        assertFalse(anyOfColor);
    }
}