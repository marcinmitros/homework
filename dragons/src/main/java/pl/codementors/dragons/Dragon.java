package pl.codementors.dragons;

public class Dragon {
    private String name;
    private int age;
    private String color;
    private int wingspan;

    public Dragon(String name, int age, String color, int wingspan) {
        this.name = name;
        this.age = age;
        this.color = color;
        this.wingspan = wingspan;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getColor() {
        return color;
    }

    public int getWingspan() {
        return wingspan;
    }
}
