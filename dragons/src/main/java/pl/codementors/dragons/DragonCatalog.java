package pl.codementors.dragons;

import java.util.*;
import java.util.stream.Collectors;

public class DragonCatalog {
    private List<Dragon> dragons;

    public DragonCatalog() {
        dragons = new ArrayList<Dragon>();
    }

    public void add(Dragon dragon) {
        dragons.add(dragon);
    }

    public void remove(Dragon dragon) {
        dragons.remove(dragon);
    }

    public void remove(int index) {
        dragons.remove(index);
    }

    public Optional<Dragon> getOldestDragon() {
        return dragons.stream()
                .max(Comparator.comparingInt(Dragon::getAge));
    }

    public OptionalInt getMaxWingspan() {
        return dragons.stream()
                .mapToInt(Dragon::getWingspan)
                .max();
    }

    public OptionalInt getMaxNameLength() {
        return dragons.stream()
                .mapToInt(dragon -> dragon.getName().length())
                .max();
    }

    public List<Dragon> getSortedByAge() {
        return dragons.stream()
                .sorted(Comparator.comparingInt(Dragon::getAge))
                .collect(Collectors.toList());
    }

    public List<Dragon> getByColor(String color) {
        return dragons.stream()
                .filter(dragon -> dragon.getColor().equals(color))
                .collect(Collectors.toList());
    }

    public List<String> getNames() {
        return dragons.stream()
                .map(Dragon::getName)
                .collect(Collectors.toList());
    }

    public List<String> getColorsToUpperCase() {
        return dragons.stream()
                .map(dragon -> dragon.getColor().toUpperCase())
                .collect(Collectors.toList());
    }

    public boolean areAllOlderThan(int age) {
        return dragons.stream()
                .allMatch(dragon -> dragon.getAge() > age);
    }

    public boolean isAnyOfColor(String color) {
        return dragons.stream()
                .anyMatch(dragon -> dragon.getColor().equals(color));
    }

}
